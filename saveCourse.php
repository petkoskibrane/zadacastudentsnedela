<?php

include 'course.php';
include 'datawriter.php';
$name = $_POST['name'];
$professor = $_POST['professor'];

$course = new Course($name,$professor);

$datawriter = new DataWriter();
$datawriter->saveCourse($course);

header("Location: indexform.php");