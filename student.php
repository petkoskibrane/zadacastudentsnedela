<?php

include_once 'printer.php';

class Student implements PrintData
{
    private $name,$address,$cources;


    public function __construct($name,$address,$cources)
    {
        $this->name = $name;
        $this->address = $address;
        $this->cources = $cources;
    }


    public function getName(){
        return $this->name;
    }

    public function getAddress(){
        return $this->address;
    }


    public function getCourses(){
        return $this->cources;
    }

    public function printData(){
        return "Student : ".$this->getName()."<br>" . $this->getCoursesInformation()."<strong>Average grade :".$this->getAverageGrade()."</strong>"."<br>";
    } 
    
    public function addStudentCourse($studentCourse)
	{
		array_push($this->cources, $studentCourse);
		
	}

    
    private function getCoursesInformation(){
        $result = "";

        foreach ($this->cources as $key => $value) {
            $result .= $value->printData()."<br>";
        }

        return $result;
    }

    private function getAverageGrade(){
        $result = 0;

        foreach ($this->cources as $key => $value) {
            $result += $value->getGrade();
        }


        return $result/(count($this->cources));
    }

    
    
}