<?php

include_once 'printer.php';


class StudentCourse implements PrintData
{
    private $course,$grade;
    public function __construct($course,$grade){
        $this->course = $course;
        $this->grade = $grade;
    }


    public function getGrade(){
        return $this->grade;
    }


    public function printData(){
        return "Course : ".$this->course->getName(). " grade ".$this->getGrade();
    }    
}