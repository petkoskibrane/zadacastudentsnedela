<?php



class Course 
{
    private $name,$professor;

    public function __construct($name,$professor){
        $this->name = $name;
        $this->professor = $professor;
    }

    public function getprofessor(){
        return $this->professor;
    }

    public function getName(){
        return $this->name;
    }


    
}