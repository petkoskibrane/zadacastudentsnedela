<?php




class ListData
{

    public function list($filename){
        $listdata = file_get_contents($filename);
        
        $listdata = explode(PHP_EOL,$listdata);
        
        $listResult = "";
        
        
        foreach ($listdata as $key => $value) {
            $listResult .= "<option>".$value."</option>";
        }


        return $listResult;
    }

}
