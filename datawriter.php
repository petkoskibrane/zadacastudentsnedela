<?php


class DataWriter
{
    public function saveStudent($student){
        file_put_contents("students.txt",$student->getName().PHP_EOL,FILE_APPEND);

    }

    public function saveCourse($course){
        file_put_contents("courses.txt",$course->getName().PHP_EOL,FILE_APPEND);

    }

    public function addPassedStudents($student,$studentCourse){
        file_put_contents("passedsStudents.txt",$student->getName().",".$studentCourse->printData().PHP_EOL,FILE_APPEND);

    }
}